#!/bin/bash                                                                                                    

# sudo -i avant.                                                                                              

if [ "$#" -ne 1 ]; then
    echo "Usage restore_bbb.sh <save_path>"
    exit;
fi

save_path=$1

if [ ! -d $save_path ]; then
    echo "Le répertoire "$save_path" n'existe pas"
    exit
fi

#restore greenlight
FILE=${save_path}/greenlight.tgz
if [ -f "$FILE" ]; then
    echo "Restore greenlight"
    cd /tmp
    rm -rf ./greenlight
    tar -zxvf ${save_path}/greenlight.tgz

    cd /root/greenlight
    docker-compose down
    rsync -a /tmp/greenlight/db /root/greenlight
    old_pwd=$(awk -F = '$1=="DB_PASSWORD" {print $2}' /tmp/greenlight/.env)
    cur_pwd=$(awk -F = '$1=="DB_PASSWORD" {print $2}' /root/greenlight/.env)
    sed -i -e "s/$cur_pwd/$old_pwd/g" /root/greenlight/.env
    docker-compose up -d
    cd
fi

#restore PHONE DIAL FILES (SIP)
FILE=${save_path}/phone_profile.xml
if [ -f "$FILE" ]; then
    echo "Restore phone dial"
    cp -f ${save_path}/phone_profile.xml /opt/freeswitch/conf/sip_profiles/external/.
    cp -f ${save_path}/phone_plan.xml /opt/freeswitch/conf/dialplan/public/.
fi

#customs default site identity and rooms
echo "Restore customs"

cp -f ${save_path}/default.pdf /var/www/bigbluebutton-default/default.pdf
cp -f ${save_path}/favicon.ico /var/www/bigbluebutton-default/favicon.ico

#customs salons
#cp -f $${save_path}/bigbluebutton.properties /usr/share/bbb-web/WEB-INF/classes/.


FILE=${save_path}/bigbluebutton.properties
if [ -f "$FILE" ]; then
    echo "Restore BBB properties"
    old_salt=$(awk -F = '$1=="securitySalt" {print $2}' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties)
    new_salt=$(awk -F = '$1=="securitySalt" {print $2}' $FILE)
    sed -i "s/$new_salt/$old_salt/g" $FILE
    mv /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties.ori
    cp $FILE /usr/share/bbb-web/WEB-INF/classes/.
fi

FILE=${save_path}/settings.yml
if [ -f "$FILE" ]; then
    cp $FILE /usr/share/meteor/bundle/programs/server/assets/app/config/.
fi


bbb-conf --restart

