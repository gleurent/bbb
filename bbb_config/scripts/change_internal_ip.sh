#!/bin/bash

new_ip=$(hostname -I | cut -f1 -d' ')
old_ip=$(awk -F = '$1=="bbb.sip.app.ip" {print $2}' /usr/share/red5/webapps/sip/WEB-INF/bigbluebutton-sip.properties)

if [ $new_ip != $old_ip ]; then
    sed -i "s/$old_ip/$new_ip/g" /usr/share/red5/webapps/sip/WEB-INF/bigbluebutton-sip.properties
    sed -i "s/$old_ip/$new_ip/g" /opt/freeswitch/etc/freeswitch/vars.xml
    sed -i "s/$old_ip/$new_ip/g" /usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml						
    bbb-conf --restart
fi;

