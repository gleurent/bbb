#!/bin/bash


if [ "$#" -ne 1 ]; then
    echo "Usage : change_ipv6_usage.sh on|off"
    exit
fi

if [ $1 == "off" ]; then
    sed -i "s/::/127.0.0.1/g" /opt/freeswitch/etc/freeswitch/autoload_configs/event_socket.conf.xml
    if [ -f /opt/freeswitch/etc/freeswitch/sip_profiles/internal-ipv6.xml ]; then
	mv /opt/freeswitch/etc/freeswitch/sip_profiles/internal-ipv6.xml /opt/freeswitch/etc/freeswitch/sip_profiles/internal-ipv6.xml_
    fi
    if [ -f /opt/freeswitch/etc/freeswitch/sip_profiles/external-ipv6.xml ]; then
	mv /opt/freeswitch/etc/freeswitch/sip_profiles/external-ipv6.xml /opt/freeswitch/etc/freeswitch/sip_profiles/external-ipv6.xml_
    fi;
    echo -e "net.ipv6.conf.all.disable_ipv6 = 1 \nnet.ipv6.conf.default.disable_ipv6 = 1 \nnet.ipv6.conf.lo.disable_ipv6 = 1" > /etc/sysctl.d/99-disable-ipv6.conf
    exit
fi

if [ $1 == "on" ]; then
    sed -i "s/127.0.0.1/::/g" /opt/freeswitch/etc/freeswitch/autoload_configs/event_socket.conf.xml
    if [ -f /opt/freeswitch/etc/freeswitch/sip_profiles/internal-ipv6.xml_ ]; then
	mv /opt/freeswitch/etc/freeswitch/sip_profiles/internal-ipv6.xml_ /opt/freeswitch/etc/freeswitch/sip_profiles/internal-ipv6.xml
    fi
    if [ -f /opt/freeswitch/etc/freeswitch/sip_profiles/external-ipv6.xml_ ]; then
	mv /opt/freeswitch/etc/freeswitch/sip_profiles/external-ipv6.xml_ /opt/freeswitch/etc/freeswitch/sip_profiles/external-ipv6.xml
    fi
    rm -f /etc/sysctl.d/99-disable-ipv6.conf
    exit
fi

echo "Unknown option : "$1
