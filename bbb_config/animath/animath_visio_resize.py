#!/usr/bin/env python3
from animath_conf import *
from animath import *

nb_cams = int( sys.argv[1] )

scw=ScalewayManager(animath_token,visio_conf['zone'])

animath_resize_default_visio(scw,nb_cams, visio_conf['url'])

