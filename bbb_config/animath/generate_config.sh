#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage generate_config.sh <token> <bbb_conf_path>"
    exit;
fi
tok=$1
pa=$2
sed "s/TOKEN/$tok/g" $pa"/animath/animath_conf_model.py" | sed "s+BBB_CONFIG_PATH+$pa+g" > $pa"/animath/animath_conf.py"
