#!/usr/bin/env python3
from animath_conf import *
from animath import *

scw=ScalewayManager(animath_token,visio_conf['zone'])
serv_visio=scw.get_server_desc(visio_conf['url'])
scw.server_start(serv_visio['id'])
animath_infos_default_visio(scw, visio_conf['url'])
