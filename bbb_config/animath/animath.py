from datetime import datetime
from scaleway import *
import time

# Animath functions

# just not to forget that once the instance is launched
# it could take several minutes before BBB runs properly

def wait_for_visio():
    """
    Give time for the BBB instance to start.
    
    Let's call that asynchronous programming.
    """
    print("Wait 2 minutes to let time to the server to start")
    for i in range(60):
        print('.',end='',flush=True)
        time.sleep(2)
    print('.',flush=True)
    print("your instance is now ready")

def find_instance(com_name):
    l=[ x for x in scaleway_instances if (x.name == com_name) ]
    if (len(l)==0):
        return(None)
    return(l[0])


def instance_visio_max_cams(inst):
    inst_type=find_instance(inst)
    return(2*inst_type.cpu)

def instance_stream_max_debit(inst):
    inst_type=find_instance(inst)
    return(inst_type.max_debit)

def instances_properties_visio(inst):
    print("Maximum of "+str(instance_visio_max_cams(inst))+" webcams")

def instances_properties_stream(inst):
    print("Maximum of "+str(instance_stream_max_debit(inst))+" Mbit/sec")

    

def animath_resize_default_visio(scw_animath,nb_cam, url):
    serv_desc=scw_animath.get_server_desc(url)
    old_instance=get_instance_by_name(serv_desc['commercial_type'])
    new_instance=instance_choice(nb_cam, old_instance)
    if  new_instance and new_instance != old_instance:
        new_serv=scw_animath.resize_server(serv_desc['id'],new_instance)
        wait_for_visio()
        return(new_serv)

def animath_back_to_default_visio(scw_animath, url):
    serv_curr=scw_animath.get_server_desc(url)
    serv_ori=scw_animath.get_server_desc_byname(url)
    if ( serv_curr['id'] != serv_ori['id'] ) :
        print("Detach IP")
        serv_ip=scw_animath.detach_ip(serv_curr)
        print("Attach IP")
        new_serv=scw_animath.attach_ip(serv_ori,serv_ip['ip'])
        print("Re-start the default server")
        new_inst=scw_animath.server_start(serv_ori)
        task=scw_animath.wait_for_state("/servers/"+new_inst['server']['id'],'running')
        scw_animath.wait_for_visio(new_inst)
        print("Terminate the resized server")
        res=scw_animath.server_terminate(scw_animath_conf,serv_curr)
        return(new_inst)
    else :
        print("The default visio is already the current visio")
        return(serv_curr)

def animath_infos_default_visio(scw_animath, url):
    serv_desc=scw_animath.get_server_desc(url)
    print("**********************************")
    print("Type : ",end=' ')
    print(serv_desc['commercial_type'])
    print("Statut : ",end=' ')
    print(serv_desc['state'])
    print("Capacité : ",end=' ')
    instances_properties_visio(serv_desc['commercial_type'])
    print("**********************************")
    return(serv_desc)

def animath_infos_default_stream(scw_animath, url):
    serv_desc= scw_animath.get_server_desc(url)
    print("**********************************")
    print("Type : ",end=' ')
    print(serv_desc['commercial_type'])
    print("Statut : ",end=' ')
    print(serv_desc['state'])
    print("Capacité : ",end=' ')
    instances_properties_stream(serv_desc['commercial_type'])
    print("**********************************")
    return(serv_desc)

