import sys
animath_token='TOKEN'
bbb_root='BBB_CONFIG_PATH'
animath_py_path=bbb_root+'/animath'
scaleway_py_path=bbb_root+'/scaleway'

visio_conf={'url':'visio.animath.live','zone':'fr-par-1'}
stream_conf={'url':'stream.animath.live','zone':'fr-par-1'}

sys.path.append(animath_py_path)
sys.path.append(scaleway_py_path)
