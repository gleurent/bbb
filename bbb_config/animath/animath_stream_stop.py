#!/usr/bin/env python3
from animath_conf import *
from animath import *

scw=ScalewayManager(animath_token,stream_conf['zone'])
serv_stream=scw.get_server_desc(stream_conf['url'])
scw.server_stop(serv_stream['id'])
animath_infos_default_stream(scw, stream_conf['url'])
