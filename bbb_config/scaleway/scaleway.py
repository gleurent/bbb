import sys, time, json
import socket, http.client, ssl

#utils

# make a header for curl commands and store also some unavoidable informations
# tok is the token for using the Scaleway API
# zo is th echosen 'zone'

class Scaleway:
    """
    Class for types of Scaleway instances. Concrete types are statically defined,
    and can be accessed from the list scaleway_instances.
    
    To get an instance, you can use:
        - get_instance_by_name(name)
        - smallest_instance()
        - largest_instance()
        - instance_choice(nb_webcams)
    """
    WEBCAM_PER_CPU = 2
    def __init__(self, name, volume_size, cpu, max_debit):
        self.name = name
        self.volume_size = volume_size
        self.cpu = cpu
        self.max_debit = max_debit
        
    def __str__(self):
        return "Instance %s:\n\tVolume: %i GB\n\tNumber of CPU: %i\n\tMax debit: %i Mbit/s" % (self.name, self.volume_size, self.cpu, self.max_debit)
    
    __repr__ = __str__
    
    def valid_instance(self, nb_cams, screen_share=False, shared_whiteboard=False):
        """
        Current heuristic:
        Requires ~1GHz per webcam. Screen share and whiteboard not taken into account.
        """
        return self.WEBCAM_PER_CPU*self.cpu >= nb_cams

# List of scaleway instances
DEV1_M = Scaleway("DEV1-M",  40,  3,   300)
DEV1_L = Scaleway("DEV1-L",  80,  4,   400)
GP1_S  = Scaleway("GP1-S" , 300,  8,   800)
GP1_M  = Scaleway("GP1-M" , 600, 16,  1500)
GP1_L  = Scaleway("GP1-L" , 600, 32,  5000)
GP1_XL = Scaleway("GP1-XL", 600, 48, 10000)

# list of possible instances, sorted by priority
scaleway_instances = [DEV1_M, DEV1_L, GP1_S, GP1_M, GP1_L, GP1_XL]

def smallest_instance() : return DEV1_M
def largest_instance() : return GP1_XL

def instance_choice(nb_webcams, screen_share=False, shared_whiteboard=False):
    """
    Provide the first instance that satisfies the given constraints
    """
    l = [ x for x in scaleway_instances if x.valid_instance(nb_webcams,screen_share=screen_share, shared_whiteboard=shared_whiteboard) ]
    if len(l) == 0:
        print("No suitable instance found")
        return None
    return l[0]

def get_instance_by_name(name):
    l = [ x for x in scaleway_instances if name == x.name ]
    if len(l) == 0:
        print("No instance found with name %s" % name)
        return None
    return l[0]

def deep_match(d, fields, value):
    if len(fields) == 0:
        return d == value
    if fields[0] in d:
        return deep_match(d[fields[0]], fields[1:], value)
    return False

def find_matching(l, fields, value) :
    if type(fields) == str:
        fields = [fields]
    sub = [x for x in l if deep_match(x, fields, value)]
    if len(sub) == 0:
        return None
    return sub[0]

class ScalewayManager():
    """
    Class to interact with Scaleway.
    
    Arguments:
        token -- the string of your secret Scaleway API token,
        zone -- the scaleway zone of the servers you want to manage.
    Valid zones are 'fr-par-1', 'fr-par-2', 'nl-ams-1', 'pl-waw-1'
    """
    def __init__(self, token, zone):
        self.token = token
        self.zone = zone

    def call_scaleway(self, page, data=None, method=None):
        """
        Makes an HTTP call to the Scaleway API.
        
        Arguments:
            page -- the API page, excluding the zone. Shall begin with a "/"
            data -- data sent to the server. If any, shall be a python dictionary
            method -- HTTP method. Defaults to GET if there is no data, and POST otherwise
        """
        context = ssl.create_default_context()
        context.minimum_version = ssl.TLSVersion.TLSv1_2
        connexion = http.client.HTTPSConnection("api.scaleway.com", context=context)
        url = "/instance/v1/zones/"+self.zone+page
        if data:
            if not method: 
                method = 'POST'
            data = json.dumps(data, separators=(',',':')).encode('utf8')
        elif not method:
            method = 'GET'
        headers = { "X-Auth-Token" : self.token, "Content-Type" : "application/json" }
        connexion.request(method, url, body=data, headers=headers)
        response = connexion.getresponse()
        body = response.read()
        connexion.close()
        if body :
            return json.loads(body.decode('utf8'))
        return None

    def list_servers(self):
        """
        Returns a python list of all servers in the zone
        """
        return self.call_scaleway("/servers")['servers']
        
    def get_server_desc(self, hostname):
        """
        Returns the server associated with the given hostname in the zone, if any.
        """
        public_ip=socket.gethostbyname(hostname)
        servers = self.list_servers()
        desc = find_matching(servers, ['public_ip', 'address'], public_ip)
        if not desc:
            print("No matching server")
        return desc

    def get_server_desc_byname(self,server_name):
        """
        Returns the server with the name server_name in the zone, if any.
        """
        servers = self.list_servers()
        desc = find_matching(servers, 'name', server_name)
        if not desc:
            print("No matching server")
        return desc

    def _do_prompt_check(self, string, resource_type, resource_id):
        data = self.call_scaleway("/%s/%s" % (resource_type, resource_id) )
        resource = data[list(data)[0]]
        print("\nYou're planning to %s\n\tname : %s\n\tid : %s\n" % (string, resource['name'], resource_id))
        a = input("Type 'YES' to proceed:")
        return a == "YES"


    def server_status(self, server_id):
        """
        Returns a python dictionary containing info about the
        server server_id
        """
        return self.call_scaleway("/servers/"+server_id)

    def server_start(self,server_id):
        """
        Starts the server with the given server_id.
        The server shall be stopped for this command to succeed.
        """
        return self.call_scaleway("/servers/"+server_id+"/action", { "action" : "poweron" })

    def server_stop(self,server_id):
        """
        Stops the server with the given server_id.
        The server shall be running for this command to succeed.
        """
        return self.call_scaleway("/servers/"+server_id+"/action", { "action" : "poweroff" })

    def delete_server(self,server_id, bypass_prompt_check=False):
        if bypass_prompt_check or self._do_prompt_check("delete the server:", "servers", server_id):
            return self.call_scaleway("/servers/"+server_id, method='DELETE')

    def server_terminate(self,server_id, bypass_prompt_check=False):
        """
        Terminate the server (delete the server and any attached volume) with the given server_id
        """
        if bypass_prompt_check or self._do_prompt_check("delete all volumes attached to server:", "servers", server_id):
            return self.call_scaleway("/servers/"+server_id+"/action", { "action" : "terminate" })

    def create_server(self, image, server_name, server_type, ext_ip_id=None, extra_volume_id=None) :
        d = { "name" : server_name, "commercial_type" : server_type.name, "project" : image['project'], "image" : image['id'], "volumes" : {}}
        if ext_ip_id:
            d["public_ip"] = ext_ip_id
        if extra_volume_id:
            d["volumes"] = { "1" : { "id" : extra_volume_id, "name" : "padding" } }
        return self.call_scaleway("/servers", d)

    def _create_padding_volume(self, size, pro):
        return self.call_scaleway("/volumes", { "name" : "padding", "size" : size, "project" : pro , "volume_type" : "l_ssd"})

    def create_image(self, name, root_volume, project):
        return self.call_scaleway("/images", { "name": name, "root_volume" : root_volume, "project" : project , "arch" : "x86_64" })

    def delete_volume(self, volume_id, bypass_prompt_check=False):
        if bypass_prompt_check or self._do_prompt_check("delete the volume:", "volumes", volume_id):
            return self.call_scaleway("/volumes/"+volume_id, method='DELETE')

    def _delete_padding_volumes(self, server, bypass_prompt_check=False):
        for v in server['volumes']:
            if server['volumes'][v]['name'] == 'padding':
                self.delete_volume(server['volumes'][v]['id'],bypass_prompt_check=bypass_prompt_check)
    
    def delete_snapshot(self, snap_id, bypass_prompt_check=False):
        if bypass_prompt_check or self._do_prompt_check("delete the snapshot:", "snapshots", snap_id):
            return self.call_scaleway("/snapshots/"+snap_id, method='DELETE')

    def delete_image(self, img_id, bypass_prompt_check=False):
        if bypass_prompt_check or self._do_prompt_check("delete the image:", "images", img_id):
            return self.call_scaleway("/images/"+img_id, method='DELETE')

    def wait_for_state(self, resource, state):
        """
        Loops until a resource switches to a given state.
        
        Effectively an infinite loop if the state is never reached.
        """
        while True:
            d = self.call_scaleway(resource)
            e = d
            while len(e) == 1 and not 'state' in e :
                e = e[list(e)[0]]
            if e['state'] == state:
                break;
            time.sleep(2)
            print('.',end='',flush=True)
        print('')
        return d

    def create_image_from_snap(self, snapshot, name="tmp-img"):
        out = self.create_image(name, snapshot['id'], snapshot['project'])
        return out['image']

    def create_server_from_image(self, image, server_name, server_type, ext_ip_id=None):
        root_size = image['root_volume']['size']
        new_size = server_type.volume_size*1_000_000_000
        padding = new_size - root_size
        if padding < 0 :
            print("Root volume too large")
            return None
        if padding == 0 :
            extra = None
        else :
            vol = self._create_padding_volume(padding, image['project'])
            self.wait_for_state("/volumes/"+vol['volume']['id'], 'available')
            extra = vol['volume']['id']
        return self.create_server(image, server_name, server_type, ext_ip_id=ext_ip_id, extra_volume_id=extra) 
    
    def detach_ip(self, ip_id):
        return self.call_scaleway("/ips/"+ip_id, { "server" : None }, method='PATCH')

    def attach_ip(self, ip_id, server_id):
        return self.call_scaleway("/ips/"+ip_id, { "server" : server_id }, method='PATCH')

    def snapshot_volume(self, volume):
        return self.call_scaleway("/snapshots", { "name": volume['name'], "volume_id": volume['id'], "project" : volume['project'] })

    def resize_server(self, server_id, new_instance):
        """
        Change the model type for a given server, reusing its IP.
        
        Arguments:
            server_id -- the string containing the id of the server to resize
            new_instance -- the new instance type. Must be of the class Scaleway()
        
        It shuts down the server, copy its root volume to the new server,
        completes with a dummy volume if needed, and start the new server.
        
        WARNING: The function does not reattach any volume besides the root volume. 
        It also deletes any attached volume named "padding".
        """
        server = self.call_scaleway("/servers/"+server_id)['server']
        root_size = int(server['volumes']['0']['size'])
        if root_size > new_instance.volume_size*1_000_000_000:
            print("Root volume too large for the requested instance")
            return None
        root_id = server['volumes']['0']['id']
        print("Stop current server")
        self.server_stop(server['id'])
        print("Wait for the server stop")
        self.wait_for_state("/servers/"+server['id'], 'stopped')
        pub_ip_id = None
        if 'public_ip' in server:
            pub_ip_id = server['public_ip']['id']
            self.detach_ip(pub_ip_id)
        print("Make a snapshot of the current server")
        s = self.snapshot_volume(server['volumes']['0'])
        s = s['snapshot']
        img = self.create_image_from_snap(s)
        print("Create the new server")
        response = self.create_server_from_image(img,server['name'], new_instance,pub_ip_id)
        new_server = response['server']
        print("Start the new server")
        self.server_start(new_server['id'])
        print("Wait for the server")
        out = self.wait_for_state("/servers/"+new_server['id'], 'running')
        print("Delete the temporary image")
        self.delete_image(img['id'], bypass_prompt_check=True)
        print("Delete the temporary snapshot")
        self.delete_snapshot(s['id'], bypass_prompt_check=True)
        print("Delete the original server")
        self.delete_server(server['id'], bypass_prompt_check=True)    
        print("Delete the volumes of the original server")
        self._delete_padding_volumes(server, bypass_prompt_check=True)
        return out



