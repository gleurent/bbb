[BigBlueButton](https://bigbluebutton.org/) is an extremely powerful open-source video conferencing software that is easy to install in a number of cases. It is not natively designed for the host server to be on a home network. Essentially it does not support changing IP addresses or fickle firewalls.

We have developed a series of scripts that complement the official script in order to propose an easy and flexible installation.

A single command whatever the situation (physical or virtual machine, with or without a firewall, behind a box or not, etc.):

> `install_bbb.sh <nom_de_machine> <adresse_mail>`

You have the choice to :
 
- download a virtual machine containing a complete installation: it will only take a few minutes to configure it and install it on your network (one to 2 minutes as long as your firewall is correctly configured)
- install an Ubuntu 16.04 linux distribution yourself, the script will then install everything you need and configure your BigBlueButton

The script will also allow you to move your installation from one machine to another without losing any data relating to your video.

The bulk of the documentation is a series of measures to ensure that any firewalls are properly configured, the actual installation only takes one line.

## Installation behind a Firewall or a Box

### Prerequisites

* A subdomain name that is pointed to the IP of the firewall or the box.
* A machine with
    * at least
        * a processor with at least 4 threads available (so a machine with 6 or 8 threads if you want to use a virtual machine)
        * 8 GB of RAM and 20 GB of disk available (so 16 GB of RAM and 40 GB of disk if you want to use a virtual machine)
    * ideally
        * a processor with at least 6 threads available (so a machine with 8 threads if you want to use a virtual machine)
        * 16 GB of RAM and 20 GB of disk available (so 32 GB of RAM and 40 GB of disk if you want to use a virtual machine)

### Configuration of NAT or Box

* Make sure that the subdomain name points to the IP of the firewall or the box.
* Make sure that it will be possible to redirect the following ports to the machine (virtual or not) when you know its local IP:
    * TCP 80, 443 and 1935
    * UDP from 16384 to 32768

## [OPTIONAL] Virtual machine installation ** _ SuperDsi _ **
You have the possibility to download a virtual machine with BigBlueButton as well as all the pre-installed scripts

### Download
Download the file (4.5 Go) https://www.animath.live/Downloads/BBB-Perso.ova

Then import the virtual machine into the virtualization software of your choice (VirtualBox for example).

### Allocated memory settings
Increase the size of memory allocated to the virtual machine to at least 8192 MB (it is 4096 MB by default for compatibility reasons). It is recommended to allocate 16MB.

### Network card configuration
Choose the network card of the host machine that is connected to your local network and switch to bridge mode.

Note in passing the mac address (or define one).

## Preparation of a machine under ubuntu 16.04 (Virtual or not)

** If you have chosen to use the _Super Dsi_ virtual machine you can skip this step. **

Otherwise, clone our directory` git https://gitlab.inria.fr/rouillie/bbb` in the `/root` directory :

> `sudo -i` 

> `git clone https://gitlab.inria.fr/rouillie/bbb`

## Installing the video

Start your machine (virtual or not).

Whatever your basic medium (virtual machine or physical machine, firewall or not, etc.), connect to an account allowing access to super-user rights.

Check that the scripts are properly installed: they should be in the `/root/bbb/bbb_config/scripts/` directory.

> For those who use the _SuperDSi_ virtual machine, the account has for login `superdsi` with the password` superdsi`.

> ** IMPORTANT ** change the password of `superdsi` ** IMMEDIATELY ** (command` passwd`) or, better ... create a superuser account then ** transfer SuperDSI ** (`userdel -r superdsi `).

** For those who do not use the _SuperDsi _ ** virtual machine: check that the `host` command is properly installed (otherwise the script will fail) and typing for example` host <your_sub_domain> `.

Locate the local IP address of your machine:

`hostname -I`


### Configuration of NAT or Box

* Redirect the following ports to the machine (virtual or not):
    * TCP 80, 443 and 1935
    * UDP from 16384 to 32768

Then type

`sudo -i`

then

`/root/bbb/bbb_config/scripts/install_bbb.sh <name_of_subdomain> <mail_address>`

### Testing your installation
If all went well, the script installed the videoconference which is now accessible: `http://<name_of_the_sub_domain>`

You can connect to it using the login `superdsi@ouaibe.fr` and the password` superdsi`.

> ** IMPORTANT ** Immediately create an administrator account then destroy the `superdsi@ouaibe.fr` account in the` greenlight` interface


### Make the installation resistant to reboot
A change of local IP makes the installation unusable. This is what usually happens when the machine is connected using DHCP.

There are 2 possibilities to correct this problem:
- or assign a static IP to the machine
- or add the line `/root/bbb/bbb_config/scripts/change_internal_ip.sh` just before` exit 0` in the file `/etc/rc.local`

### Using IPV6

The use of IPV6 can cause many problems in France on consumer networks and we have made it activatable / deactivatable on demand.

`/root/bbb/bbb_config/change_ipv6_usage.sh on` to activate the use of IPV6
`/root/bbb/bbb_config/change_ipv6_usage.sh off` to disable the use of IPV6

> On the virtual machine we offer, IPV6 is ** deactivated **

## Migrate an installation
The objective is to recover the accounts but also the complete configuration (except records) to change the machine.

### On the source machine
Log in then switch to super-user with the command

`sudo -i`

Start the backup by specifying an email address

`/root/bbb/bbb_config/scripts/save_bbb.sh <a directory>`

this will store all the files needed for account management and configuration, including SSL certificates in `<a directory>`

### On the target machine

It is assumed that the target machine is ready to receive the installation of the video (or is already fully installed with the video)

Log in then switch to super-user with the command

`sudo -i`

Recover the backup directory and copy it to `<a directory>`

Start the reconfiguration script

`/root/bbb/bbb_config/scripts/restore_bbb.sh <a directory>`
