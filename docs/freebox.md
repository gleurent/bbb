# Utilisateurs Free 

Si votre serveur est derrière la box, il faut s'assurer que le nom de votre sous-domaine pointe sur l'IP public de la box.

Pour vous connecter à l'interface d'administration de votre Freebox, tapez <https://mafreebox.freebox.fr/>

## Ajout d'une adresse IP statique

Pour ne pas que la machine change d'adresse IP à chaque démarrage, il est préférable de fixer l'adresse IP locale du serveur avec l'adresse MAC.

Pour la VM, récupérez l'adresse MAC dans la configuration réseau de la VM sur VirtualBox.

![virtualbox-network](images/virtualbox-network.png)

A partir de l'interface d'administration de la Freebox, cliquer sur "**Paramètres de la Freebox**"

![fbx-params](images/fbx-params.png)

puis  "**DHCP**" 

![fbx-dhcp](images/fbx-dhcp.png)

Sur la nouvelle page, dans la sous-section "**Baux statiques**", cliquez sur "**Ajouter un bail statique**" et associez l'adresse MAC et l'adresse IP statique que vous souhaitez.

![fbx-static-ip](images/fbx-static-ip.png)

## Redirection de ports

A partir de l'interface d'administration de la Freebox, cliquer sur "**Gestion des ports**".

![fbx-ports](images/fbx-ports.png)

Sur la nouvelle page, vous avez maintenant deux possibilités:

1. Activer la DMZ (solution la plus simple)
2. Activer la redirection de ports

### DMZ

Il suffit tout simplement d'activer la DMZ et de pointer vers l'IP du serveur (e.g., 192.168.1.20 dans mon cas).

![fbx-dmz](images/fbx-dmz.png)

### NAT & PAT

Une autre solution est de rediriger seulement les ports nécessaires pour Big Blue Button.

Les ports suivants doivent être redirigés vers l'IP locale du serveur.

- TCP 80 , 443
- UDP de 16384 à 32768

![fbx-nat](images/fbx-nat.png)

