# Utilisateurs BBox

Si votre serveur est derrière la box, il faut s'assurer que le nom de votre sous-domaine pointe sur l'IP public de la box.

Pour vous connecter à l'interface d'administration de votre BBox, tapez <https://mabbox.bytel.fr/> ou <http://192.168.1.254/>

## Ajout d'une adresse IP statique

Pour ne pas que la machine change d'adresse IP à chaque démarrage, il est préférable de fixer l'adresse IP locale du serveur avec l'adresse MAC.

Pour la VM, récupérez l'adresse MAC dans la configuration réseau de la VM sur VirtualBox.

![virtualbox-network](images/virtualbox-network.png)

A partir de l'[interface d'administration](https://www.assistance.bouyguestelecom.fr/internet-bbox/installation-bbox/parametrage-interface-administration-bbox#bbox) de la BBox, cliquer sur "**Services de la Box**" > "**DHCP**" depuis le bloc bleu BBox.

![bbox-services-dhcp](images/bbox_services-dhcp.png)

Sur la nouvelle page, dans la sous-section "**Attribution d'adresse IP statique**", ajoutez l'équipement pour associer l'adresse MAC et l'adresse IP statique que vous souhaitez.

![bbox-static-ip](images/bbox-static-ip.png)

## Redirection de ports

A partir de l'[interface d'administration](https://www.assistance.bouyguestelecom.fr/internet-bbox/installation-bbox/parametrage-interface-administration-bbox#bbox) de la BBox, cliquer sur "**Services de la Box**" > "**Redirection de ports**" depuis le bloc bleu BBox.

![bbox-services-ports](images/bbox-services-ports.png)

Sur la nouvelle page, vous avez maintenant deux possibilités:

1. Activer la DMZ (solution la plus simple)
2. Activer le NAT/PAT pour la redirection de ports

### DMZ

Il suffit tout simplement d'activer la DMZ et de pointer vers l'IP du serveur (e.g., 192.168.1.20 dans mon cas).

![bbox-dmz](images/bbox-dmz.png)

### NAT & PAT

Une autre solution est de rediriger seulement les ports nécessaires pour Big Blue Button.

Les ports suivants doivent être redirigés vers l'IP locale du serveur.

- TCP 80 , 443
- UDP de 16384 à 32768

![bbox-nat](images/bbox-nat.png)

## Gestion du sous-domaine

La BBox offre un service DynDNS vous permettant d'associer dynamiquement un nom de domaine à l'adresse IP de votre box qui peut elle aussi changer.

La Bbox supporte les services suivants:

- dyndns
- no-ip
- ovh
- duiadns
- changeip
- duckdns
